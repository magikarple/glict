import discord, asyncio, aiohttp, re

client = discord.Client()

with open('gitlab token.txt', 'r') as f:
    PRIVATE_TOKEN = f.read().strip()

URL = 'https://www.gitlab.com/api/v4'
USER_ID = 0
PROJECT_IDs = {}


@client.event
async def on_ready():
    global USER_ID, PROJECT_IDs
    print('Connected to DiscordApp!')
    async with aiohttp.ClientSession(headers={'Private-Token': PRIVATE_TOKEN}) as session:
        async with session.get(URL+'/user') as r:
            print(f'GitLab Response: {r.status}')
            if r.status != 200:
                print(await r.text())
                print('Error getting info from GitLab:\nMake sure that the token has permissions to read stuff.')
            else:
                print('Connected to GitLab!\nCaching info...')
                USER_ID = int((await r.json())['id'])
                async with session.get(f'{URL}/users/{USER_ID}/projects') as r:
                    for project in await r.json():
                        PROJECT_IDs[project['path']] = project['_links']['issues']
                print(PROJECT_IDs)
                print('Ready!\n\n')

@client.event
async def on_message(message):
    match = re.search(r'((?:https://(?:www\.)?)?gitlab\.com\S(\S+))', message.content)
    if match:
        info = match.group(2).split('/')
        if info[2] == 'issues' and info[1] in PROJECT_IDs:
            async with aiohttp.ClientSession(headers={'Private-Token': PRIVATE_TOKEN}) as session:
                async with session.get(f'{PROJECT_IDs[info[1]]}/{info[3]}') as r:
                    r = await r.json()
                    await message.channel.send(f'__{r["title"]}__\n```\nIssue #{r["iid"]}\nStatus: {r["state"].title()}\nComments: {r["user_notes_count"]}```\n{r["description"]}')

with open('discord bot token.txt', 'r') as f:
    client.run(f.read().strip())