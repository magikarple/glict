# GitLab Issue Context Thing (GLICT)

### Requirements:
- Python 3.6.x
- [discord.py](https://discordpy.readthedocs.io/en/latest/)

### Setup:
`gitlab token.txt` should only contain a GitLab token. The token must have api permission enabled and be associated with a GitLab account that has access to the repo you wish to view.
`discord bot token.txt` should only contain a DiscordApp bot token. The bot account requires read and write permissions in the server that you plan on using.

### Other Stuff:
idk, have fun lol